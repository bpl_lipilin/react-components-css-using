import React, { Component } from 'react';

import { Button, createProvider } from '@mlipilin/react-css-components';

import './app.css';

const ThemeAlfaProvider = createProvider(
    'alfa',
    () => {
        require('themes/alfa/index.css');
    },
    true,
);

const ThemeAbsolutProvider = createProvider(
    'absolut',
    () => {
        require('themes/absolut/index.css');
    },
    true,
);

const TYPES = ['default', 'primary', 'success', 'danger', 'info', 'warning'];
const SIZES = ['l', 'm', 's', 'xs'];

class App extends Component {
    render() {
        return (
            <div className="App">
                <h1 className="App__title">React Components CSS</h1>
                <ThemeAlfaProvider>
                    <fieldset>
                        <legend>Alfa</legend>
                        {TYPES.map(type => (
                            <div key={type} className="App__group">
                                {SIZES.map(size => (
                                    <Button size={size} type={type}>
                                        <span className="App__button-title">
                                            {type} {size}
                                        </span>
                                    </Button>
                                ))}
                            </div>
                        ))}
                    </fieldset>
                </ThemeAlfaProvider>

                <ThemeAbsolutProvider>
                    <fieldset>
                        <legend>Absolut</legend>
                        {TYPES.map(type => (
                            <div key={type} className="App__group">
                                {SIZES.map(size => (
                                    <Button rounded size={size} type={type}>
                                        <span className="App__button-title">
                                            {type} {size}
                                        </span>
                                    </Button>
                                ))}
                            </div>
                        ))}
                    </fieldset>
                </ThemeAbsolutProvider>
            </div>
        );
    }
}

export default App;
